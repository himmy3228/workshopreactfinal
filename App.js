/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {Provider as MovieContextProvider} from './MovieContext';
import {Provider as FaveriteContextProvider} from './FaveriteContext';

import SearhBarScreen from './SearchBarScreen';
import MovieLists from './MovieLists';
import MovieDetail from './MovieDetail';
import FavouriteLists from './FavouriteLists';

const Stack = createStackNavigator();

const App = () => {
  return (
    <FaveriteContextProvider>
      <MovieContextProvider>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="SearchScreen">
            <Stack.Screen name="SearchScreen" component={SearhBarScreen} />
            <Stack.Screen name="MovieLists" component={MovieLists} />
            <Stack.Screen name="MovieDetail" component={MovieDetail} />
            <Stack.Screen name="FavouriteLists" component={FavouriteLists} />
          </Stack.Navigator>
        </NavigationContainer>
      </MovieContextProvider>
    </FaveriteContextProvider>
  );
};

export default App;
