import React, {useContext} from 'react';
import {Context} from './FaveriteContext';
import {FlatList, TouchableHighlight} from 'react-native-gesture-handler';
import styles from './styles';
import { Text, Image, View } from 'react-native';

const FavouriteLists = props => {
  const {state} = useContext(Context);
  const {navigation} = props;
  //console.log('props', props)

  return (
    <FlatList
      style={styles.container}
      data={state}
      keyExtractor={item => item.id.toString()}
      renderItem={({item}) => {
        return (
          <TouchableHighlight
            onPress={() => navigation.navigate('MovieDetail', {data: item})}>
            <View style={styles.containerMovielist}>
              <Image
                source={{
                  uri: `https://image.tmdb.org/t/p/w92${item.poster_path}`,
                }}
                style={styles.imageSize}
              />
              <Text style={styles.setTextTitleList}>{item.title}</Text>
              <Text style={styles.setTextDate}>{item.release_date}</Text>
              <Text style={styles.setTextTitle} numberOfLines={2}>
                {item.overview}
              </Text>
            </View>
          </TouchableHighlight>
        );
      }}
    />
  );
};
export default FavouriteLists;
