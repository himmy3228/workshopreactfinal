import React, {createContext, useReducer} from 'react';

export default (reducer, actions, initialState) => {
  const Context = createContext();

  const Provider = ({children}) => {
    const [favstate, dispatch] = useReducer(reducer, initialState);

    const boundActions = {};
    for (var key in actions) {
      boundActions[key] = actions[key](dispatch);
    }

    return (
      <Context.Provider value={{state: favstate, ...boundActions}}>
        {children}
      </Context.Provider>
    );
  };

  return {Context, Provider};
};
