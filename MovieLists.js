import {Text, View, Image, Modal, Alert, Button} from 'react-native';
import React, {useState, useEffect} from 'react';
import {FlatList, TouchableHighlight} from 'react-native-gesture-handler';
import  styles  from './styles';
import api from './APIManager';
import { useNavigation } from '@react-navigation/native';

const MovieLists = (props) => {
    const {title} = props.route.params
    const [dataMovie, setDataMovie] = useState([])
    const navigation = useNavigation()

    const fetchMovie = async () => {
        const searchState = title
        const page = 1
        const response = await api.get(`/api/movies/search?query=${searchState}&page=${page}`);
        setDataMovie(response.data.results);
        
    }

    useEffect(() => {
        fetchMovie()
    },[])
    
    return (
        <FlatList 
            style = {styles.container}
            data = {dataMovie}
            keyExtractor = {item => item.id.toString()}
            renderItem = {({item}) => {
                return (
                    <TouchableHighlight
                        onPress = {() => navigation.navigate('MovieDetail', {data: item})}>
                        <View style = {styles.containerMovielist}>
                            <Image 
                                source = {{uri: `https://image.tmdb.org/t/p/w92${item.poster_path}`}}
                                style = {styles.imageSize}
                            />
                            <Text style = {styles.setTextTitleList}>{item.title}</Text>
                            <Text style = {styles.setTextDate}>{item.release_date}</Text>
                            <Text style = {styles.setTextTitle} numberOfLines = {2}>{item.overview}</Text>
                            
                        </View>  
                    </TouchableHighlight>
                )
            }}
        />
    )
}

export default MovieLists;