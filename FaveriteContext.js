import React, {useState} from 'react';
import createContext from './createContext.js';

const faveriteLists = [];

//Reducer
const movieReducer = (favstate, {type, payload}) => {
  switch (type) {
    case 'add_faverite':
      return [...favstate, payload]; 

    case 'delete_faverite':
      return favstate.filter((BlogPost) => BlogPost.id != payload)
    default:
      return favstate;
  }
};

//Actions
const addfaverite = dispatch => {
  return movie => {
    dispatch({type: 'add_faverite', payload: movie});
  };
};

const deletefaverite = dispatch => {
  return movie => {
    dispatch({type: 'delete_faverite', payload: movie});
  };
};

export const {Context, Provider} = createContext(
  movieReducer,
  {addfaverite, deletefaverite},
  faveriteLists,
);
