import React, {useContext, useState} from 'react';
import {View, Button, Text} from 'react-native';
import {Context as MovieContext} from './MovieContext';
import styles from './styles';
import {TextInput} from 'react-native-gesture-handler';
import {useNavigation} from '@react-navigation/native';
import {FlatList, TouchableHighlight} from 'react-native-gesture-handler';

const SearchBar = () => {
  const [movieName, setMovieName] = useState();
  const {state, addMovie} = useContext(MovieContext);
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <View>
        <TextInput
          style={{
            height: 60,
            borderColor: 'gray',
            borderWidth: 4,
            fontSize: 20,
          }}
          onChangeText={text => setMovieName(text)}
          value={movieName}
        />
      </View>
      <Button
        title="Search"
        onPress={() => {
          navigation.navigate('MovieLists', {title: movieName});
          addMovie(movieName);
        }}
      />
      <Button 
        title="Favourite"
        onPress={ () => {
          navigation.navigate('FavouriteLists')
        }}
      />
      <FlatList
        data={state}
        inverted={true}
        keyExtractor={item => item.id.toString()}
        renderItem={({item}) => {
          return (
            <TouchableHighlight
              style={styles.container}
              onPress={() => {
                navigation.navigate('MovieLists', {title: item.title});
              }}>
              <Text style = {styles.setTextTitleList}>{item.title}</Text>
            </TouchableHighlight>
          );
        }}
      />
    </View>
  );
};

export default SearchBar;
