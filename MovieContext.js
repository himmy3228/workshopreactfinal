import React, {useState} from 'react';
import createContext from './createContext.js';

const initialState = [];

//Reducer
const movieReducer = (state, {type, payload}) => {
  switch (type) {
    case 'add_movie':
      return [
        ...state,
        {
          id: state.length + 1,
          title: payload.title,
        },
      ];

    case 'delete_movie':
      //return state.filter((AddBlogPost) => AddBlogPost.id != payload)
      return state;
    default:
      return state;
  }
};

//Actions
const addMovie = dispatch => {
  return title => {
    dispatch({type: 'add_movie', payload: {title}});
  };
};

export const {Context, Provider} = createContext(
  movieReducer,
  {addMovie},
  initialState,
);
