import {Text, View, Image, Modal, Alert, Button} from 'react-native';
import React, {useState, useEffect, useCallback, useContext} from 'react';
import styles from './styles';
import {Context as FaveriteContext} from './FaveriteContext';

const MovieDetail = props => {
  const {data} = props.route.params;
  const {state, addfaverite, deletefaverite} = useContext(FaveriteContext);
  //favourite_LIST_State
  const [favourite, setFavourite] = useState();

  props.navigation.setOptions({
    title: 'MovieDetail',
  });
  
  const addAndDeleteFavourite = () => {
    if(favourite) {
      deletefaverite(data.id)
      setFavourite(false)
    } else {
      addfaverite(data)
      setFavourite(true)
    }
  }
  console.log('data : ', data)
  const checkFavourite = () => {
    const favouriteItem = state.find((item) => item.id === data.id)

    if (favouriteItem === undefined) {
      console.log('favI if : ', favouriteItem)
      setFavourite(false)
    } else if (favouriteItem === data) {
      console.log('favI else : ', favouriteItem)
      setFavourite(true)
    }
  }

  useEffect (() => {
    checkFavourite()
  },[])

  return (
    <View>
      <Image
        source={{uri: `https://image.tmdb.org/t/p/w92${data.poster_path}`}}
        style={styles.imageDetailSize}
      />
      <Text style={[styles.setTextTitleList, {marginLeft: 8}]}>
        {' '}
        {data.title}{' '}
      </Text>
      <Text style={[styles.setTextTitle]}>
        Average Vote : {data.vote_average}{' '}
      </Text>
      <Text> </Text>
      <Text style={[styles.setTextTitle]}> {data.overview} </Text>

      <Button 
        title= {favourite ? "Unfavourite" : "Favourite"} 
        onPress={() => {
          addAndDeleteFavourite()
        }}
      />
    </View>
  );
};

export default MovieDetail;
