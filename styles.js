import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'gray',
  },

  setTextTitle: {
    fontSize: 16,
  },

  setTextDate: {
    fontSize: 16,
    color: 'gray',
  },


  containerMovielist: {
    margin: 4,
    padding: 4,
    flexDirection: 'column',
    alignItems: 'flex-start',
    backgroundColor: '#FFFFFF',
  },

  containerMovieText: {
    margin: 4,
    padding: 4,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#FFFFFF',
  },


  imageSize: {
    width: 60,
    height: 100,
  },

  setTextTitleList: {
    padding: 5,
    fontWeight: 'bold',
    fontSize: 24,
  },

  imageDetailSize: {
    width: 120,
    height: 200,
    alignItems: 'center',
  },

});

export default styles;
